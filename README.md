# dotnet Web Service - Fibonacci function

The following is a simple dotnet Web Service who implements a classic *Fibonacci* function.

## Tools

To made this service, I used *Visual Studio* in its *Community* version. You can download it [from here][vs-cmmnt].

## Creative process

The mental path that I followed, was the following:

1. `Ctrl+Shift+N` to create a new project, and select *ASP.NET Web Application (.NET Framework)*
2. Choose a name for the project and the solution (change the *Location* if you want). I used `.NET Framework 2.0` for this project
3. Over the application, `Add` a `New Item...`, and choose `Web Service (ASMX)` putting it a descriptive name
4. Change the `[WebMethod]` included in the template, or add a new one to make your *Fibonacci* implementation

### Fibonacci sequence and code

In Maths, the *Fibonacci Sequence* is the numeric series where a **number** is found adding **two numbers before it**, starting with `0` and `1`:

`X_n = X_{n-1} + X_{n-2}`

Example: What is the *Fibonnaci* of **5**?

* `n_1 = 1`
* `n_2 = 1`
* `n_3 = 2`
* `n_4 = 3`
* `n_5 = 5`

So, to **Fibonacci of 5**, the `n` value is `5` and the `n-1` value is `3`. A simple code to do this can be the following:

```C
[WebMethod]
public string fibonacci(int n)
{
        int a = 0, b = 1, temp = 0;
        for (int i = 0; i < n; i++)
        {
                temp = a;
                a = b;
                b = temp + a;
        }
        return "<n>" + a.ToString() + "</n>" + Environment.NewLine + "<n-1>" + temp.ToString() + "</n-1>";
}
```

### Publishing the service with IIS

To publish your service using Visual Studio, follow this steps:

1. Go to `Build`, and choose `Build Solution`
2. The Output should show something like this:

```bash
> ------ Build started: Project: ws_fibo, Configuration: Release Any CPU ------
>  ws_fibo -> C:\Users\PavezDan\Documents\code\dotnet_ws_fibonacci\ws_fibo_sol\ws_fibo\bin\ws_fibo.dll
> ========== Build: 1 succeeded, 0 failed, 0 up-to-date, 0 skipped ==========
```

3. As you can see, there is a path with a `.dll` file. Now, open the IIS Manager and select the Server/Computer to see the `Home` site
4. Open the `ISAPI and CGI Restrictions`, and `Add...` the `.dll` file. In the `Description` section, put a short and descriptive name (I put `ws_fibo`). You need to check the `Allow extension path to execute` checkbox
5. Now, in the `Connections` tree, go to `<YourServerName>`, `Sites`, `Default Web Site`. Right click on it and `Add Application...`. Write a short `Alias` (I put `ws_fibo`), in `Application pool` select the Framework you chose when you create the application (remember that I chose the `.NET Framework 2.0`? It needs to match with that), and in the `Physical path`, select the folder who contains your application code
6. As you can see, the new site was created. Right click on it, `Edit Permissions...`, `Security` tab, `Edit...`, `Add...`, `Advanced...` and select the `Users` located `In Folder` with the name `<YourServer>` or `<YourMachine>`. Give `Full control` (for testing purposes) and `OK` everything

And that's all, your service is now ready to be consumed for an application. You can test the code made in this tutorial [here][ws-test].

Some test cases we can use to validate the implementation could be the following:

* **Fibonacci with n=5**: [http://200.14.215.30/ws_fibo/ws.asmx/fibonacci?n=5][n=5]
* **Fibonacci with n=15**: [http://200.14.215.30/ws_fibo/ws.asmx/fibonacci?n=15][n=15]
* **Fibonacci with n=25**: [http://200.14.215.30/ws_fibo/ws.asmx/fibonacci?n=25][n=25]

As you can see, you only need to specify the parameter in the URL, or use the operator [WebSite provided][ws-test] by the Web Service.

[ws-test]: http://200.14.215.30/ws_fibo/ws.asmx?op=fibonacci
[n=5]: http://200.14.215.30/ws_fibo/ws.asmx/fibonacci?n=5
[n=15]: http://200.14.215.30/ws_fibo/ws.asmx/fibonacci?n=15
[n=25]: http://200.14.215.30/ws_fibo/ws.asmx/fibonacci?n=25
[vs-cmmnt]: https://visualstudio.microsoft.com/downloads/
