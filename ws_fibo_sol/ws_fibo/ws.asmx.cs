﻿using System;
using System.Web.Services;

namespace ws_fibo
{
	[WebService(Namespace = "http://200.14.215.30/ws_fibo/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]

	public class WebService_fibo : System.Web.Services.WebService
	{

		[WebMethod]
		public string fibonacci(int n)
		{
			int a = 0, b = 1, temp = 0;

			for (int i = 0; i < n; i++)
			{
				temp = a;
				a = b;
				b = temp + a;
			}

			return "<n>" + a.ToString() + "</n>" + Environment.NewLine + "<n-1>" + temp.ToString() + "</n-1>";
		}
	}
}